import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';


// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ActivityService } from './activity.service';

describe('ActivityService', () => {

    let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  let service: ActivityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(ActivityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
