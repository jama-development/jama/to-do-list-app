import { Component } from '@angular/core';
import { LoadingService } from 'src/app/utils/services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent  {

  isLoading$ = this.loadingService.isLoading$;
  // Just One Exception
  constructor(private loadingService: LoadingService) {}

}
